// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4 (swiftlang-1205.0.26.9 clang-1205.0.19.55)
// swift-module-flags: -target i386-apple-ios10.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name OZLivenessSDK
import AVFoundation
import Accelerate
import CommonCrypto
import Compression
import CoreData
import CoreFoundation
import CoreGraphics
import CoreImage
import Dispatch
import Foundation
import ImageIO
import MobileCoreServices
import Swift
import SystemConfiguration
import TensorFlowLiteC
import UIKit
import VideoToolbox
import simd
public struct ResponseError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
public struct Features : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum LicenseError : Swift.Int, Swift.Error {
  case invalidLicenseObjectData
  case inv101
  case inv102
  case inv103
  case inv104
  case inv105
  case inv106
  case inv107
  case invalidAppId
  case licenseExpired
  case inv110
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension LicenseError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
}
extension UIImage {
  convenience public init?(pixelBuffer: CoreVideo.CVPixelBuffer)
}
extension CGImage {
  public static func create(pixelBuffer: CoreVideo.CVPixelBuffer) -> CoreGraphics.CGImage?
}
public struct OZCustomization {
  @available(*, deprecated, message: "Use centerHintCustomization")
  public var textColor: UIKit.UIColor
  public var buttonColor: OZLivenessSDK.OZButtonColors
  @available(iOS, renamed: "faceFrameCustomization")
  public var ovalCustomization: OZLivenessSDK.OZFaceFrameCustomization {
    get
    set
  }
  public var faceFrameCustomization: OZLivenessSDK.OZFaceFrameCustomization
  public var centerHintCustomization: OZLivenessSDK.OZCenterHintCustomization
  public var customCloseButton: UIKit.UIButton?
}
public struct OZButtonColors {
  public var darkColor: UIKit.UIColor
  public var lightColor: UIKit.UIColor
}
public struct OZCenterHintCustomization {
  public var textColor: UIKit.UIColor
  public var textFont: UIKit.UIFont?
  public var textPosition: Swift.Int?
}
public struct OZFaceFrameCustomization {
  public enum GeometryType {
    case oval
    case rect(cornerRadius: CoreGraphics.CGFloat)
  }
  public var strokeWidth: CoreGraphics.CGFloat
  public var successStrokeColor: UIKit.UIColor
  public var failStrokeColor: UIKit.UIColor
  public var geometryType: OZLivenessSDK.OZFaceFrameCustomization.GeometryType
}
public enum AnalysisType {
  case quality, document, biometry, blackList
  public static var all: Swift.Set<OZLivenessSDK.AnalysisType> {
    get
  }
  public static func == (a: OZLivenessSDK.AnalysisType, b: OZLivenessSDK.AnalysisType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum AnalysisMode {
  case onDevice, serverBased, hybrid
  public static func == (a: OZLivenessSDK.AnalysisMode, b: OZLivenessSDK.AnalysisMode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct Analysis {
  public init(media: [OZLivenessSDK.OZMedia]?, type: OZLivenessSDK.AnalysisType, mode: OZLivenessSDK.AnalysisMode, params: [Swift.String : Any]?)
  public init(media: [OZLivenessSDK.OZMedia]?, type: OZLivenessSDK.AnalysisType, mode: OZLivenessSDK.AnalysisMode)
}
public struct AnalysisStatus {
  public let media: OZLivenessSDK.OZMedia
  public let index: Swift.Int
  public let from: Swift.Int
  public let progress: Foundation.Progress
  public let cancelableRequest: OZLivenessSDK.OZCancelableRequest
}
public enum LicenseSource {
  case licenseFilePath(Swift.String)
}
public struct LicenseData {
  public var appIDS: [Swift.String]
  public var expires: Foundation.TimeInterval
  public var features: OZLivenessSDK.Features
  public var configs: OZLivenessSDK.ABTestingConfigs?
}
public struct OZFrameCustomization {
  public var backgroundColor: UIKit.UIColor
}
@available(iOS 11.0, *)
public var OZSDK: OZLivenessSDK.OZSDKProtocol {
  get
}
public func OZSDK(licenseSources: [OZLivenessSDK.LicenseSource], completion: @escaping ((OZLivenessSDK.LicenseData?, OZLivenessSDK.LicenseError?) -> Swift.Void))
public func AnalysisRequestBuilder(_ folderId: Swift.String? = nil) -> OZLivenessSDK.OZRequestProtocol
public typealias FaceCaptureCompletion = ([OZLivenessSDK.OZMedia]?, OZLivenessSDK.OZVerificationStatus?) -> Swift.Void
public protocol OZRequestProtocol : AnyObject {
  func addAnalysis(_ analysis: OZLivenessSDK.Analysis)
  func addMedia(_ media: OZLivenessSDK.OZMedia)
  func addMedia(_ media: [OZLivenessSDK.OZMedia])
  func addFolderId(_ folderId: Swift.String)
  func addFolderMeta(_ meta: [Swift.String : Any])
  @discardableResult
  func run(scenarioProgressHandler: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), uploadProgressHandler: @escaping ((OZLivenessSDK.AnalysisStatus) -> Swift.Void), completionHandler: @escaping (OZLivenessSDK.AnalysisResult?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest?
}
public protocol OZSDKProtocol : AnyObject {
  var resultsObserver: (([OZLivenessSDK.OZVerificationResult]) -> Swift.Void) { get set }
  var journalObserver: ((Swift.String) -> Swift.Void) { get set }
  var licenseResult: ((OZLivenessSDK.LicenseData?, OZLivenessSDK.LicenseError?) -> Swift.Void) { get set }
  var localizationCode: OZLivenessSDK.OZLocalizationCode? { get set }
  var host: Swift.String { get }
  var attemptSettings: OZLivenessSDK.OZAttemptSettings { get set }
  var customization: OZLivenessSDK.OZCustomization { get set }
  var version: Swift.String { get }
  var licenseData: OZLivenessSDK.LicenseData? { get }
  @discardableResult
  func login(_ login: Swift.String, password: Swift.String, completion: @escaping (Swift.String?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  func isLoggedIn() -> Swift.Bool
  func logout()
  func setPermanentAccessToken(_ accessToken: Swift.String)
  @available(*, deprecated, message: "Use createVerificationVCWithDelegate with OZLivenessDelegate")
  func createVerificationVCWithDelegate(_ delegate: OZLivenessSDK.OZVerificationDelegate, actions: [OZLivenessSDK.OZVerificationMovement]) throws -> UIKit.UIViewController
  @available(*, deprecated, message: "Use createVerificationVCWithDelegate with OZLivenessDelegate")
  func createVerificationVCWithDelegate(_ delegate: OZLivenessSDK.OZVerificationDelegate, actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position) throws -> UIKit.UIViewController
  func createVerificationVCWithDelegate(_ delegate: OZLivenessSDK.OZLivenessDelegate, actions: [OZLivenessSDK.OZVerificationMovement]) throws -> UIKit.UIViewController
  func createVerificationVCWithDelegate(_ delegate: OZLivenessSDK.OZLivenessDelegate, actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], completion: @escaping OZLivenessSDK.FaceCaptureCompletion) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position, completion: @escaping OZLivenessSDK.FaceCaptureCompletion) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], completion: @escaping ([OZLivenessSDK.OZVerificationResult]) -> Swift.Void) throws -> UIKit.UIViewController
  func createVerificationVC(actions: [OZLivenessSDK.OZVerificationMovement], cameraPosition: AVFoundation.AVCaptureDevice.Position, completion: @escaping ([OZLivenessSDK.OZVerificationResult]) -> Swift.Void) throws -> UIKit.UIViewController
  func prepareVerificationStruct(image: UIKit.UIImage, movement: OZLivenessSDK.OZVerificationMovement) -> OZLivenessSDK.OZVerificationResult
  func prepareVerificationStruct(media: OZLivenessSDK.OZVideo) -> OZLivenessSDK.OZVerificationResult
  @discardableResult
  func analyse(results: [OZLivenessSDK.OZVerificationResult], analyseStates: Swift.Set<OZLivenessSDK.OZAnalysesState>, fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func analyse(folderId: Swift.String, results: [OZLivenessSDK.OZVerificationResult], analyseStates: Swift.Set<OZLivenessSDK.OZAnalysesState>, fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  func runOnDeviceLivenessAnalysis(result: OZLivenessSDK.OZVerificationResult, completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Error?) -> Swift.Void)
  func runOnDeviceLivenessAnalysis(result: OZLivenessSDK.OZVerificationResult, completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Float?, Swift.Error?) -> Swift.Void)
  func runOnDeviceLivenessAnalysis(results: [OZLivenessSDK.OZVerificationResult], completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Float?, Swift.Error?) -> Swift.Void)
  func runOnDeviceBiometryAnalysis(firstResult: OZLivenessSDK.OZVerificationResult, secondResult: OZLivenessSDK.OZVerificationResult, completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Error?) -> Swift.Void)
  func runOnDeviceBiometryAnalysis(firstResult: OZLivenessSDK.OZVerificationResult, secondResult: OZLivenessSDK.OZVerificationResult, completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, Swift.Float?, Swift.Error?) -> Swift.Void)
  @discardableResult
  func addToFolder(results: [OZLivenessSDK.OZVerificationResult], fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (Swift.String?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func addToFolder(folderId: Swift.String, results: [OZLivenessSDK.OZVerificationResult], fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (Swift.String?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func addToFolder(results: [OZLivenessSDK.OZVerificationResult], metadata: [Swift.String : Any]?, fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (Swift.String?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @available(*, deprecated, message: "Use documentAnalyse without analyseStates")
  @discardableResult
  func documentAnalyse(documentPhoto: OZLivenessSDK.DocumentPhoto, results: [OZLivenessSDK.OZVerificationResult], analyseStates: Swift.Set<OZLivenessSDK.OZAnalysesState>, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func documentAnalyse(documentPhoto: OZLivenessSDK.DocumentPhoto, results: [OZLivenessSDK.OZVerificationResult], analysisTypes: Swift.Set<OZLivenessSDK.AnalysisType>, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func documentAnalyse(documentPhoto: OZLivenessSDK.DocumentPhoto, results: [OZLivenessSDK.OZVerificationResult], scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func uploadAndAnalyse(results: [OZLivenessSDK.OZVerificationResult], documentPhoto: OZLivenessSDK.DocumentPhoto?, analysisTypes: Swift.Set<OZLivenessSDK.AnalysisType>, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func uploadAndAnalyse(results: [OZLivenessSDK.OZVerificationResult], documentPhoto: OZLivenessSDK.DocumentPhoto?, analysisTypes: Swift.Set<OZLivenessSDK.AnalysisType>, metadata: [Swift.String : Any]?, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func uploadAndAnalyse(folderId: Swift.String, results: [OZLivenessSDK.OZVerificationResult], documentPhoto: OZLivenessSDK.DocumentPhoto?, analysisTypes: Swift.Set<OZLivenessSDK.AnalysisType>, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  @discardableResult
  func uploadAndAnalyse(folderId: Swift.String?, media: [OZLivenessSDK.OZMedia], analysisTypes: Swift.Set<OZLivenessSDK.AnalysisType>, scenarioState: @escaping ((OZLivenessSDK.ScenarioState) -> Swift.Void), fileUploadProgress: @escaping ((Foundation.Progress) -> Swift.Void), completion: @escaping (OZLivenessSDK.AnalyseResolutionStatus?, [OZLivenessSDK.AnalyseResolution]?, Swift.Error?) -> Swift.Void) -> OZLivenessSDK.OZCancelableRequest
  func cleanTempDirectory()
  func configure(with host: Swift.String)
  func set(licenseBundle: Foundation.Bundle) throws
  func set(licenseBundle: Foundation.Bundle, licenseSources: [OZLivenessSDK.LicenseSource])
  func setLicense(from path: Swift.String)
}
public protocol OZCancelableRequest {
  func cancel()
}
public struct DocumentPhoto {
  public init(front: Foundation.URL?, back: Foundation.URL?)
  public var front: Foundation.URL?
  public var back: Foundation.URL?
}
public enum OZAnalysesState : Swift.String {
  case liveness
  case quality
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
@available(*, deprecated, message: "Use OZLivenessDelegate")
public protocol OZVerificationDelegate : AnyObject {
  func onOZVerificationResult(results: [OZLivenessSDK.OZVerificationResult])
}
public protocol OZLivenessDelegate : AnyObject {
  func onOZLivenessResult(results: [OZLivenessSDK.OZMedia])
  func onError(status: OZLivenessSDK.OZVerificationStatus?)
}
public struct OZVerificationResult {
  public var status: OZLivenessSDK.OZVerificationStatus {
    get
  }
  public var movement: OZLivenessSDK.OZVerificationMovement {
    get
  }
  public var videoURL: Foundation.URL? {
    get
  }
  public var bestShotURL: Foundation.URL? {
    get
  }
  public var preferredMediaURL: Foundation.URL? {
    get
  }
  public var timestamp: Foundation.Date {
    get
  }
}
public struct OZLivenessResult {
  public var status: OZLivenessSDK.OZVerificationStatus {
    get
  }
  public var media: OZLivenessSDK.OZMedia
}
public enum MediaType {
  case movement
  case documentBack
  case documentFront
  public static func == (a: OZLivenessSDK.MediaType, b: OZLivenessSDK.MediaType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct OZMedia {
  public init(movement: OZLivenessSDK.OZVerificationMovement?, mediaType: OZLivenessSDK.MediaType, metaData: [Swift.String : Any]?, videoURL: Foundation.URL?, bestShotURL: Foundation.URL?, preferredMediaURL: Foundation.URL?, timestamp: Foundation.Date)
  public init(mediaType: OZLivenessSDK.MediaType, metaData: [Swift.String : Any]?, preferredMediaURL: Foundation.URL?, timestamp: Foundation.Date)
  public var movement: OZLivenessSDK.OZVerificationMovement?
  public var mediaType: OZLivenessSDK.MediaType
  public var metaData: [Swift.String : Any]?
  public var videoURL: Foundation.URL?
  public var bestShotURL: Foundation.URL?
  public var preferredMediaURL: Foundation.URL?
  public var timestamp: Foundation.Date
}
public struct OZVideo {
  public init(movement: OZLivenessSDK.OZVerificationMovement, videoURL: Foundation.URL?, bestShotURL: Foundation.URL?, preferredMediaURL: Foundation.URL?, timestamp: Foundation.Date)
  public var movement: OZLivenessSDK.OZVerificationMovement
  public var videoURL: Foundation.URL?
  public var bestShotURL: Foundation.URL?
  public var preferredMediaURL: Foundation.URL?
  public var timestamp: Foundation.Date
}
public enum OZVerificationStatus {
  case userProcessedSuccessfully
  case userNotProcessed
  case failedBecauseUserCancelled
  case failedBecauseCameraPermissionDenied
  case failedBecauseOfBackgroundMode
  case failedBecauseOfTimeout
  case failedBecauseOfAttemptLimit
  case failedBecausePreparingTimout
  case failedBecauseOfLowMemory
  public static func == (a: OZLivenessSDK.OZVerificationStatus, b: OZLivenessSDK.OZVerificationStatus) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct OZAttemptSettings {
  public var faceAlignmentTimeout: Swift.Double?
  public init(singleCount: Swift.Int? = nil, commonCount: Swift.Int? = nil, faceAlignmentTimeout: Swift.Double? = nil)
}
public enum OZVerificationMovement {
  @available(*, deprecated, message: "Deprecated")
  case close
  @available(*, deprecated, message: "Deprecated")
  case far
  case smile
  case eyes
  case scanning
  case selfie
  case one_shot
  case left
  case right
  case down
  case up
  public static func == (a: OZLivenessSDK.OZVerificationMovement, b: OZLivenessSDK.OZVerificationMovement) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public class AnalyseResolution {
  public var status: OZLivenessSDK.AnalyseResolutionStatus
  public var type: Swift.String
  public var folderID: Swift.String?
  public var score: Swift.Float?
  public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?)
  public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?, score: Swift.Float?)
  @objc deinit
}
public class AnalysisResult {
  public var status: OZLivenessSDK.AnalyseResolutionStatus
  public var analysis: [OZLivenessSDK.AnalyseResolution]
  public init(status: OZLivenessSDK.AnalyseResolutionStatus, analysis: [OZLivenessSDK.AnalyseResolution])
  @objc deinit
}
@_inheritsConvenienceInitializers public class DocumentAnalyseResolution : OZLivenessSDK.AnalyseResolution {
  public func getTextValue(by fieldName: Swift.String) -> Swift.String?
  override public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?)
  override public init(type: Swift.String, status: OZLivenessSDK.AnalyseResolutionStatus, folderID: Swift.String?, score: Swift.Float?)
  @objc deinit
}
public enum AnalyseResolutionStatus : Swift.String {
  case initial
  case processing
  case failed
  case finished
  case declined
  case success
  case operatorRequired
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum ScenarioState {
  case addToFolder, addAnalyses, waitAnalysisResult
  public static func == (a: OZLivenessSDK.ScenarioState, b: OZLivenessSDK.ScenarioState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum OZLocalizationCode {
  case ru, en, hy, kk, ky, tr
  public static func == (a: OZLivenessSDK.OZLocalizationCode, b: OZLivenessSDK.OZLocalizationCode) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum OZJournalEvent : Swift.String {
  case unknown
  case livenessSessionInitialization
  case livenessCheckStart
  case actionStart
  case actionRestart
  case actionFinish
  case actionFailed
  case livenessCheckFinish
  case actionFacePositionFixed
  case actionFacePositionRecommendation
  case actionRecordStart
  case actionRecordFinish
  case actionRecordSaved
  case brightnessChanged
  case error
  case request
  case response
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public struct ABTestingConfigs : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
extension OZLivenessSDK.OZLocalizationCode : Swift.Equatable {}
extension OZLivenessSDK.OZLocalizationCode : Swift.Hashable {}
extension OZLivenessSDK.OZVerificationMovement : Swift.Equatable {}
extension OZLivenessSDK.OZVerificationMovement : Swift.Hashable {}
extension OZLivenessSDK.OZJournalEvent : Swift.Equatable {}
extension OZLivenessSDK.OZJournalEvent : Swift.Hashable {}
extension OZLivenessSDK.OZJournalEvent : Swift.RawRepresentable {}
extension OZLivenessSDK.LicenseError : Swift.Equatable {}
extension OZLivenessSDK.LicenseError : Swift.Hashable {}
extension OZLivenessSDK.LicenseError : Swift.RawRepresentable {}
extension OZLivenessSDK.AnalysisType : Swift.Equatable {}
extension OZLivenessSDK.AnalysisType : Swift.Hashable {}
extension OZLivenessSDK.AnalysisMode : Swift.Equatable {}
extension OZLivenessSDK.AnalysisMode : Swift.Hashable {}
extension OZLivenessSDK.OZAnalysesState : Swift.Equatable {}
extension OZLivenessSDK.OZAnalysesState : Swift.Hashable {}
extension OZLivenessSDK.OZAnalysesState : Swift.RawRepresentable {}
extension OZLivenessSDK.MediaType : Swift.Equatable {}
extension OZLivenessSDK.MediaType : Swift.Hashable {}
extension OZLivenessSDK.OZVerificationStatus : Swift.Equatable {}
extension OZLivenessSDK.OZVerificationStatus : Swift.Hashable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.Equatable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.Hashable {}
extension OZLivenessSDK.AnalyseResolutionStatus : Swift.RawRepresentable {}
extension OZLivenessSDK.ScenarioState : Swift.Equatable {}
extension OZLivenessSDK.ScenarioState : Swift.Hashable {}
